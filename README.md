
# My personal awesomelist with awesome tools and articles about systems integrations and all other awesome things

Inspired by the [awesome](https://github.com/sindresorhus/awesome) list thing.
The following list is a little bit more awesome as other awesome lists ;-), it's also a knowledge base and compilation of articles.

## Other awesome lists

[awesome-cli-apps](https://github.com/agarrharr/awesome-cli-apps) - Selection of awesome commandline tools

[awesome-shell](https://github.com/alebcay/awesome-shell)

[awesome-zsh-plugins](https://github.com/unixorn/awesome-zsh-plugins)

[awesome-ssh](https://github.com/moul/awesome-ssh)

[awesomw-aws](https://github.com/donnemartin/awesome-aws)


## Tools

[The Fuck](https://github.com/nvbn/thefuck) - Corrects the last commandline input

[tldr](https://github.com/tldr-pages/tldr) - Alternative and better readable manpages of commands

[rtop](https://github.com/rapidloop/rtop) - Remote top to monitoring systems over a remote connection

[clusterssh](https://github.com/duncs/clusterssh) - SSH client to manage several hosts at once

[PAW](https://paw.cloud/client) - API development, testing and so on

[m-cli](https://github.com/rgcr/m-cli) - Manage macOS over commandline

[SAWS](https://github.com/donnemartin/saws#installation) - Advanced AWS cli, based on the aws-cli

[Decktape](https://github.com/astefanutti/decktape) - PDF Export for HTML presentations, such as reveal.js presentation

[zsh-autosuggestion](https://github.com/zsh-users/zsh-autosuggestions) - Autosuggestion for the zsh

[WebPagetest](https://github.com/WPO-Foundation/webpagetest) - Webpage performance testing

[Postman](https://www.getpostman.com/apps) - API development and testing suite

[newman](https://github.com/postmanlabs/newman) - The cli companion for postman

[websequencediagrams](https://www.websequencediagrams.com/) - Online Tool to create sequence diagrams in different styles

[AWLESS](https://github.com/wallix/awless) - Alternative AWS CLI in golang

[TestSSL](https://github.com/drwetter/testssl.sh) - CLI to test TLS/SSL Cipher Suites from your local command line

[stress-ng](https://www.tecmint.com/linux-cpu-load-stress-test-with-stress-ng-tool/) - Stress test tool for UNIX systems

## Articles

### Agile

[Spotify Retro Kit](https://labs.spotify.com/2017/12/15/spotify-retro-kit/) - Retrospectives at Spotify

[Retromat](https://plans-for-retrospectives.com/de/) - Ideas for Retrospectives in Agile Environments

[The Tweetrospective](http://www.hamvocke.com/blog/tweetrospective-agile-retrospective/) - A Retrospective with tweets

[Painless Bug Tracking](https://www.joelonsoftware.com/2000/11/08/painless-bug-tracking/) - Description and Example of good bug reporting/tracking

[Healthy Team Backlogs](https://www.ebayinc.com/stories/blogs/tech/healthy-team-backlogs/) - Backlog management at ebay (includes a checklist for healthy backlogs)

[Weighted Shortest Job First](http://www.scaledagileframework.com/wsjf/) - Prioritization model for sequence jobs

[The Zombie Apocalypse Retrospective](https://www.ebayinc.com/stories/blogs/tech/the-zombie-apocalypse-retrospective/) - Retrospective as a zombie scenario

[A peer feedback system for scrum teams](https://www.ebayinc.com/stories/blogs/tech/now-you-see-it-a-peer-feedback-system-for-scrum-teams/) - Individual performance in a scrum team by survey other team members

[Splitting user stories -- the hamburger method](https://gojko.net/2012/01/23/splitting-user-stories-the-hamburger-method/) - A method to slice user stories

### Info Sec

[OAuth 2 Simplified](https://aaronparecki.com/oauth-2-simplified/) - Good introduction to OAuth 2

[Solutions for using OAuth 2.0 for Authentication](http://www.thread-safe.com/2012/01/solutions-for-using-oauth-20-for.html) - Discussion of solution to use OAuth for authentication

[OpenID Connect in a nutshell](https://nat.sakimura.org/2012/01/20/openid-connect-nutshell/) - Introduction to OpenID Connect

[How does SSL and TLS work](https://security.stackexchange.com/questions/20803/how-does-ssl-tls-work) - Introduction to SSL/TLS

[OAUTH RECOMMENDATIONS FOR SINGLE-PAGE APPS](https://www.pingidentity.com/en/company/blog/posts/2018/oauth-recommendations-for-single-page-apps.html) - Use of OAuth in SPA context

[SECURELY USING THE OIDC AUTHORIZATION CODE FLOW AND A PUBLIC CLIENT WITH SINGLE PAGE APPLICATIONS](https://www.pingidentity.com/en/company/blog/posts/2018/securely-using-oidc-authorization-code-flow-public-client-single-page-apps.html) - Sample architecture for SPA with OAuth

### DevOps

[DevOps Wiki](https://github.com/Leo-G/DevopsWiki) - Starting point to different tutorials

[DevOps Weekly](https://www.devopsweekly.com/) - Weekly newsletter with articles regarding DevOps (Tools, Blog articles)

[SRE Weekly](https://sreweekly.com/) - Weekly newsletter with articles regarding SRE

[How to become a devops engineer for dummies](https://techblog.shutl.com/2016/06/how-to-become-a-devops-engineer-for-dummies/) - Little story about on-call support for systems (also provides a checklist for a gap analysis)

[Jenkins Pipeline Beginners Guide](https://medium.com/@mightywomble/jenkins-pipeline-beginners-guide-f3868f715ed9)

### SysAdmin, Ops

[Scalable and secure access with SSH](https://code.facebook.com/posts/365787980419535/scalable-and-secure-access-with-ssh/) - Access to production systems at Facebook

[Replacing Modern Tools With Retro Linux Commands](https://dzone.com/articles/replacing-modern-tools-with-retro-linux-commands)

[How to Load Test SAML SSO Secured Websites with JMeter](https://www.blazemeter.com/blog/how-load-test-saml-sso-secured-websites) - Loadtest on SAML SSO resources

[Java - Hanging Thread Detection and Handling](http://www.javaworld.com/article/2074310/java-app-dev/java-hanging-thread-detection-and-handling.html) - Handling hanging threads on JVM

[Troubleshooting Hanging or Looping Processes](http://www.oracle.com/technetwork/java/javase/hangloop-140257.html)

[Of Thread dumps and stack traces …](http://www.0xcafefeed.com/2004/06/of-thread-dumps-and-stack-traces/) - Using thread dumps with the JVM

[Different ways to implement flags in bash](http://jonalmeida.com/posts/2013/05/26/different-ways-to-implement-flags-in-bash/)- Flags in Bash scripts

[Unix Cheatsheet](https://rubytune.com/cheat/) - A brief cheat sheet for the most used commands in operation of an unix machine

[Introduction to modern network load balancing and proxying](https://blog.envoyproxy.io/introduction-to-modern-network-load-balancing-and-proxying-a57f6ff80236) - Good introduction towards load balancing

[The Evolution of Code Deploys at Reddit](https://redditblog.com/2017/06/02/the-evolution-of-code-deploys-at-reddit/) - Nice history of how deployments are done at Reddit

### Dev

[Commit Often, Perfect Later, Publish Once: Git Best Practices](https://sethrobertson.github.io/GitBestPractices/) - Selection of git best practices

[A Note About Git Commit ](http://tbaggery.com/2008/04/19/a-note-about-git-commit-messages.html) - Writting good commit messages

[Setup MacBook Pro for developers](https://clakech.github.io/macbook-pro-setup/)

[Postman: Test your REST API](http://thecuriousdev.org/postman-test-your-rest-api/) - Introduction to Postman

[Vim Cheat Sheet Tutorial](http://www.viemu.com/a_vi_vim_graphical_cheat_sheet_tutorial.html) - Tutorial/Cheat Sheet for vim

[Vim Cheat Sheet for Programmers](http://michael.peopleofhonoronly.com/vim/) - Cheat Sheet for vim in different designs

### Cloud Computing

[Amazon Web Services — a practical guide](https://github.com/open-guides/og-aws) - A good starting point for AWS tips & tricks

### Misc

[Web Developer Roadmap](https://github.com/kamranahmedse/developer-roadmap) - A guideline for technologies to learn

[Collection of DevOps job interview questions](https://github.com/Leo-G/DevopsWiki/wiki/Devops-Interview-Questions) - Interview questions for all devops related topics

[Blog of Robert Broeckelmann](https://medium.com/@robert.broeckelmann) - Blog about API management and securing APIs

## Tutorials & Dojos & Example Implementations & SDKs

[Commandline Dojo Training](https://www.shortcutfoo.com/app/dojos/command-line)

[Commandline Training](https://cmdchallenge.com/#/count_string_in_line)

[AppAuth for iOS and macOS](https://github.com/openid/AppAuth-iOS)

[PingIdentity Angular SPA sample app](https://github.com/pingidentity/angular-spa-sample)